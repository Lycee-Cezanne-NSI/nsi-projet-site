---
author: JL Harel
title: Chapitre 1
---

## I. Paragraphe 1 :

!!! example "Exemple"

    ```pycon
    >>> premiere(3)
    6
    ```

### 1. Sous paragraphe 1

???+ question "Compléter le code ci-dessous"

    ```python
    def addition(a,b):
        c=a+b
        return
    ```

### 2. Sous paragraphe 2

Texte 1.2

## II. Paragraphe 2 : Quelques formules

Utiliser LaTeX

### 1. En maths

Une suite : 

$$
\left\{
        \begin{array}{ll}
            u_0 = 77 \\
            u_{n+1} = 5 \times u_n+2\\
        \end{array}
\right.
$$

Ajouter ses commandes :

$$
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}
\norm{\vec{v_C}} = \frac{\sqrt{(x_D - x_C)^2 + (y_D - y_C)^2}}{\Delta t}
$$

La norme du vecteur ${\vec{u}}$ se note $\norm{\vec{u}}$.

### 2. En chimie

$$
{CuSO_4}_{(s)}   \rightarrow   {Cu^{2+}}_{(aq)}+ {SO_{4}^{2-}}_{(aq)}
$$

$$
^{14}_{6}C  \rightarrow \ ^{14}_{7}N  + \  ^{ 0}_{-1}e^{-}
$$

On peut tout mettre en ligne : d'abord cette formule ${CuSO_4}_{(s)}   \rightarrow   {Cu^{2+}}_{(aq)}+ {SO_{4}^{2-}}_{(aq)}$ 
puis celle-ci :  $^{14}_{6}C  \rightarrow \ ^{14}_{7}N  + \  ^{ 0}_{-1}e^{-}$
