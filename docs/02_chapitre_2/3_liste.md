---
author: JL Harel
title: Premier élément d'une liste Python
tags:
  - liste/tableau
---

La fonction `premier_liste` prend en paramètre une liste Python **non vide** et renvoie le premier élément de cette liste

???+ question "Compléter le code ci-dessous"

    {{ IDE('scripts/premier_liste') }}
